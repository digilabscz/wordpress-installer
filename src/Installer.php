<?php declare(strict_types=1);

namespace Digilabscz\WordPressInstaller;

use Composer\Installer\PackageEvent;
use Nette\Utils\FileSystem;

class Installer
{
    /**
     * @param PackageEvent $event
     */
    public static function postPackageInstall(PackageEvent $event): void
    {
        $packageName = $event->getOperation()->getPackage()->getName();
        if ($class = self::getPluginClassName($packageName)) {
            $class::install();
        }
    }

    /**
     * @param PackageEvent $event
     */
    public static function prePackageUninstall(PackageEvent $event): void
    {
        $packageName = $event->getOperation()->getPackage()->getName();
        if ($class = self::getPluginClassName($packageName)) {
            $class::uninstall();
        }
    }

    /**
     * @param PluginConfiguration $configuration
     */
    public static function installPlugin(PluginConfiguration $configuration): void
    {
        $root = self::getRootDir();
        $alias = $configuration->getPluginAlias();
        $path = $root . '/wp-content/plugins/' . $alias .'/' . $alias .'.php';

        $content = '<?php' . PHP_EOL;
        $content .= $configuration->generateAnnotation();
        $content .= PHP_EOL;
        $content .= 'if (! class_exists(' . $configuration->getPluginClassname() . '::class)) {' . PHP_EOL;
        $content .= '    require_once(__DIR__ . \'/../../../vendor/autoload.php\');' . PHP_EOL;
        $content .= '}' . PHP_EOL;
        $content .= $configuration->getPluginClassname() . '::init();' . PHP_EOL;

        FileSystem::write($path, $content);

        require_once $root . '/wp-load.php';
        require_once $root . '/wp-admin/includes/plugin.php';

        $plugin = $alias . '/' . $alias . '.php';
        if (! is_plugin_active($plugin) ) {
            activate_plugin($plugin);
        }
    }

    /**
     * @param PluginConfiguration $configuration
     */
    public static function uninstallPlugin(PluginConfiguration $configuration): void
    {
        $root = self::getRootDir();

        require_once $root . '/wp-load.php';
        require_once $root . '/wp-admin/includes/plugin.php';

        $alias = $configuration->getPluginAlias();
        $plugin = $alias . '/' . $alias . '.php';
        if (is_plugin_active($plugin) ) {
            deactivate_plugins($plugin);
        }

        FileSystem::delete($root . '/wp-content/plugins/' . $alias . '/');
    }

    /**
     * @param string $packageName
     * @return string|IPlugin|null
     */
    private static function getPluginClassName(string $packageName): ?string
    {
        $pluginFile = self::getRootDir() . '/vendor/' . $packageName . '/src/Plugin.php';
        if (! file_exists($pluginFile)) {
            return null;
        }

        $classes = get_declared_classes();
        include $pluginFile;
        $classes = array_values(array_diff_key(get_declared_classes(), $classes));

        if (empty($classes)) {
            return null;
        }

        $class = reset($classes);
        if (! is_subclass_of($class, IPlugin::class)) {
            return null;
        }

        return $class;
    }

    /**
     * @return string
     */
    private static function getRootDir(): string
    {
        static $dir;
        if ($dir === null) {
            $dir = dirname(__FILE__, 5);
        }

        return $dir;
    }
}
