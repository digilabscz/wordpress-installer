<?php declare(strict_types=1);

namespace Digilabscz\WordPressInstaller;

interface IPlugin
{
    public static function install(): void;

    public static function uninstall(): void;

    public static function init(): void;
}
