<?php declare(strict_types=1);

namespace Digilabscz\WordPressInstaller;

use InvalidArgumentException;
use Nette\Utils\Strings;

class PluginConfiguration
{
    /**
     * @var string
     */
    private string $pluginClassname;

    /**
     * @var string
     */
    private string $pluginAlias;

    /**
     * @var string
     */
    private string $pluginName = '';

    /**
     * @var string
     */
    private string $pluginUri = '';

    /**
     * @var string
     */
    private string $description = '';

    /**
     * @var string
     */
    private string $version = '1.0';

    /**
     * @var string
     */
    private string $requiresAtLeast = '5.8.1';

    /**
     * @var string
     */
    private string $requiresPHP = '7.4';

    /**
     * @var string
     */
    private string $author = '';

    /**
     * @var string
     */
    private string $authorUri = '';

    /**
     * @var string
     */
    private string $license = '';

    /**
     * @var string
     */
    private string $licenseUri = '';

    /**
     * @var string
     */
    private string $updateUri = '';

    /**
     * @var string
     */
    private string $textDomain = '';

    /**
     * @param string $pluginClassname
     * @param string $pluginAlias
     */
    public function __construct(string $pluginClassname, string $pluginAlias = '')
    {
        if (! is_subclass_of($pluginClassname, IPlugin::class)) {
            throw new InvalidArgumentException('`' . $pluginClassname . '` must implement interface `' . IPlugin::class . '`!');
        }

        $this->pluginClassname = $pluginClassname;
        $this->pluginAlias = $pluginAlias;
    }

    /**
     * @return string
     */
    public function getPluginClassname(): string
    {
        return $this->pluginClassname;
    }

    /**
     * @return string
     */
    public function getShortPluginClassname(): string
    {
        $parts = explode('\\', $this->pluginClassname);

        return end($parts);
    }

    /**
     * @return string
     */
    public function getPluginAlias(): string
    {
        if ($this->pluginAlias) {
            return $this->pluginAlias;
        }

        return Strings::webalize(strtolower(
            preg_replace('/(?!^)[[:upper:]][[:lower:]]/', '$0',
                preg_replace('/(?!^)[[:upper:]]+/', '-$0', $this->getShortPluginClassname())
            )
        ));
    }

    /**
     * @param string $pluginName
     * @return $this
     */
    public function setPluginName(string $pluginName): self
    {
        $this->pluginName = $pluginName;

        return $this;
    }

    /**
     * @param string $pluginUri
     * @return $this
     */
    public function setPluginUri(string $pluginUri): self
    {
        $this->pluginUri = $pluginUri;

        return $this;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param string $version
     * @return $this
     */
    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @param string $requiresAtLeast
     * @return $this
     */
    public function setRequiresAtLeast(string $requiresAtLeast): self
    {
        $this->requiresAtLeast = $requiresAtLeast;

        return $this;
    }

    /**
     * @param string $requiresPHP
     * @return $this
     */
    public function setRequiresPHP(string $requiresPHP): self
    {
        $this->requiresPHP = $requiresPHP;

        return $this;
    }

    /**
     * @param string $author
     * @return $this
     */
    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @param string $authorUri
     * @return $this
     */
    public function setAuthorUri(string $authorUri): self
    {
        $this->authorUri = $authorUri;

        return $this;
    }

    /**
     * @param string $license
     * @return $this
     */
    public function setLicense(string $license): self
    {
        $this->license = $license;

        return $this;
    }

    /**
     * @param string $licenseUri
     * @return $this
     */
    public function setLicenseUri(string $licenseUri): self
    {
        $this->licenseUri = $licenseUri;

        return $this;
    }

    /**
     * @param string $updateUri
     * @return $this
     */
    public function setUpdateUri(string $updateUri): self
    {
        $this->updateUri = $updateUri;

        return $this;
    }

    /**
     * @param string $textDomain
     * @return $this
     */
    public function setTextDomain(string $textDomain): self
    {
        $this->textDomain = $textDomain;

        return $this;
    }

    /**
     * @return string
     */
    public function generateAnnotation(): string
    {
        $fields = [];
        if ($this->pluginName) {
            $fields['Plugin Name'] = $this->pluginName;
        }
        if ($this->pluginUri) {
            $fields['Plugin URI'] = $this->pluginUri;
        }
        if ($this->description) {
            $fields['Description'] = $this->description;
        }
        if ($this->version) {
            $fields['Version'] = $this->version;
        }
        if ($this->requiresAtLeast) {
            $fields['Requires at least'] = $this->requiresAtLeast;
        }
        if ($this->requiresPHP) {
            $fields['Requires PHP'] = $this->requiresPHP;
        }
        if ($this->author) {
            $fields['Author'] = $this->author;
        }
        if ($this->authorUri) {
            $fields['Author URI'] = $this->authorUri;
        }
        if ($this->license) {
            $fields['License'] = $this->license;
        }
        if ($this->licenseUri) {
            $fields['License URI'] = $this->licenseUri;
        }
        if ($this->updateUri) {
            $fields['Update URI'] = $this->updateUri;
        }
        if ($this->textDomain) {
            $fields['Text Domain'] = $this->textDomain;
        }

        $output = '/**' . PHP_EOL;
        foreach ($fields as $property => $value) {
            $output .= ' * ' . $property . ': ' . $value . PHP_EOL;
        }
        $output .= ' */' . PHP_EOL;

        return $output;
    }
}
